<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://paulbriar.com
 * @since      1.0.0
 *
 * @package    Wp_Starter_Cleanup
 * @subpackage Wp_Starter_Cleanup/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Wp_Starter_Cleanup
 * @subpackage Wp_Starter_Cleanup/includes
 * @author     Paul Briar <pbriar@live.com>
 */
class Wp_Starter_Cleanup_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
