<?php

/**
 * Fired during plugin activation
 *
 * @link       https://paulbriar.com
 * @since      1.0.0
 *
 * @package    Wp_Starter_Cleanup
 * @subpackage Wp_Starter_Cleanup/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Wp_Starter_Cleanup
 * @subpackage Wp_Starter_Cleanup/includes
 * @author     Paul Briar <pbriar@live.com>
 */
class Wp_Starter_Cleanup_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
