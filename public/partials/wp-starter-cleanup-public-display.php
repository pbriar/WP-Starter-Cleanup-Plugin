<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://paulbriar.com
 * @since      1.0.0
 *
 * @package    Wp_Starter_Cleanup
 * @subpackage Wp_Starter_Cleanup/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
